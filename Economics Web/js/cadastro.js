$('#cadastrar').click(function(e){
		e.preventDefault();
		var nome = $('#user');
		var email = $('#email');
		var sexo = $("input[name='sexo']:checked");
		var senha= $('#senha');
		var confsenha = $('#confsenha');

		if(nome.val()=='' || senha.val()=='' || email.val()=='' || confsenha.val()=='' || sexo.val()== null){
			swal({
  				title: "Preencha todos os campos!",
  				text: "todos os campos são obrigatórios",
  				icon: "warning"
			});

			return false;
		}
		if(senha.val()!=confsenha.val()){
			swal({
  				title: "Senha não corresponde!",
  				text: "Senha não é a mesma que a confirmação dela",
  				icon: "warning"
			});

			return false;	
		}
		$.ajax({
			url:'controle/inserirUser.php',
			method:'POST',
			datatype:"html",
			data:{
				nome: nome.val(),
				email: email.val(),
				sexo: sexo.val(),
				senha: senha.val()
			},
			success: function (result) {
				if(result==1){
					nome.val("");
					email.val("");
					sexo.val("");
					senha.val("");
					window.location.href="pf.php";
				}else{
					swal({
  						title: "Algum erro ocorreu",
  						text: "Por favor, tente novamente.",
  						icon: "warning"
					});
					senha.val('');
					confsenha.val('');

				}
				
      		}
		});
	});