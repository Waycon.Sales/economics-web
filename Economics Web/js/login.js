$('#btnenvia').click(function(e){
		e.preventDefault();
		var user = $('#user');
		var senha= $('#senha');
		if(user.val()=='' || senha.val()==''){
			swal({
  				title: "Preencha todos os campos!",
  				text: "todos os campos são obrigatórios",
  				icon: "warning"
			});

			return false;
		}
		$.ajax({
			url:'controle/valida-login.php',
			method:'POST',
			datatype:"html",
			data:{
				user: user.val(),
				senha: senha.val()
			},
			success: function (result) {
				if(result==1){
					user.val('');
					senha.val('');
					window.location.href="pf.php";
				}else{
					swal({
  						title: "Usuario ou senha incorretos",
  						text: "Por favor, tente novamente.",
  						icon: "warning"
					});
					senha.val('');

				}
				
      		}
		});
	});
