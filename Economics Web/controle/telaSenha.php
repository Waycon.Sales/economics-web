<?php
session_start();
if($_SESSION['nome']){


echo '   
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Historico PF</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/editSenha.css">
</head>
<body>
<div class="voltar container-fluid"> 
	<a href="Perfil.php"> <- Voltar </a>
</div>
	<section>
		<div class="container-fluid">
			<div  class="container his">
			<form action="editSenha.php" method="post">
				<div class=" txt-sd text-center mb-4">
					<h3 class="txt-saudacao">Alterar Senha</h3>
				</div>
				<div class="form-group">
				    <label for="senha" class="font">Senha</label>
					<input type="password" class="form-control" name="senha" id="senha"  placeholder="Digite sua senha"/>
				</div>
				<div class="form-group">
				    <label for="nome" class="font">Confirme a senha</label>
					<input type="password" class="form-control" name="confsenha" id="confsenha"  placeholder="Corfirmação de senha"/>
				</div>
				<div class="form-group">
	  				<input type="submit" id="editar" class="btn text-center" value="Editar"/>
	  			</div>	
			</div>
		</div>
	</section>';

	require_once("../footer.php");
echo '   
</body>
</html>

';
}else{
	header("Location: ../login.php");
}

?>
