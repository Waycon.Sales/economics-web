<?php
require_once("Conect.php");
require_once("../modelo/planFM.php");
class PlanControle{
	
	
	function remover($id){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("DELETE FROM plano WHERE id=:id;");
                $cmd->bindParam("id", $id);
                if($cmd->execute()){
                    return true;
                }else{
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro de PDO: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        
        
        function selecionarTodos(){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM plano;");
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "PlanoFinanceiro");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }

        function selecionarTodosPoridUser($id){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM plano WHERE userPlano=:id;");
                $cmd->bindParam("id", $id);
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "PlanoFinanceiro");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        
        
        function selecionarPid($id){
            try{
                $conexao = new Conexao();	
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM plano WHERE id=:id;");
                $cmd->bindParam("id", $id);
                $cmd->execute();
                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        
        
        function inserir($pf){
            try{
                $conexao = new Conexao();
				$ca = $pf->getCapital();
				$c = $pf->getContas();
				$p = $pf->getPoupanca();
				$u = $pf->getUserPlano();
				$f = $pf->getFutilidades();
				$i = $pf->getInvestimento();
				$e = $pf->getEmepro();
				$d = $pf->getData();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO plano(capital,contas,poupanca,userPlano,futilidades,investimento,emepro,data) values(:ca,:c,:p,:u,:f,:i,:e,:d);");
                $cmd->bindParam("ca", $ca);
                $cmd->bindParam("c", $c);
                $cmd->bindParam("p", $p);
                $cmd->bindParam("u", $u);
                $cmd->bindParam("f", $f);
                $cmd->bindParam("i", $i);
                $cmd->bindParam("e", $e);
                $cmd->bindParam("d", $d);
				if($cmd->execute()){
					return true;
				}else{
					return false;
				}
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        
        
        function update($pf){
            try{
                $conexao = new Conexao();
				$cap = $pf->getCapital();
				$co = $pf->getContas();
				$po = $pf->getPoupanca();
				$us = $pf->getUserPlano();
				$fu = $pf->getFutilidades();
				$in = $pf->getInvestimento();
				$em = $pf->getEmepro();
				$da = $pf->getData();
                $cmd = $conexao->getConexao()->prepare("UPDATE plano SET capital = :cap,contas = :co,poupanca = :po,userPlano =:us,futilidades = :fu,investimento = :in,emepro = :em,data = :da;");
                $cmd->bindParam("cap", $cap);
                $cmd->bindParam("co", $co);
                $cmd->bindParam("po", $po);
                $cmd->bindParam("us", $us);
                $cmd->bindParam("fu", $fu);
                $cmd->bindParam("in", $in);
                $cmd->bindParam("em", $em);
                $cmd->bindParam("da", $da);            
				if($cmd->execute()){
					return true;
				}else{
					return false;
				}
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
}


?>
