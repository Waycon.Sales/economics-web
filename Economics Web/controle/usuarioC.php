<?php
require_once("Conect.php");
require_once("../modelo/usuarioM.php");
class UsuarioControle{


		function remover($id){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("DELETE FROM usuario WHERE id=:id;");
                $cmd->bindParam("id", $id);
                if($cmd->execute()){
                    return true;
                }else{
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro de PDO: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }



        function selecionarTodos(){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario;");
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Usuario");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        
        
        function selecionarPid($id){
            try{
                $conexao = new Conexao();	
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario WHERE id=:id;");
                $cmd->bindParam("id", $id);
                $cmd->execute();
                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        
        
        function update($usuario){
            try{
                $conexao = new Conexao();
				$nome = $usuario->getNome();
				$email = $usuario->getEmail();
				$sexo = $usuario->getSexo();
                $cmd = $conexao->getConexao()->prepare("UPDATE usuario SET nome = :n,email = :e,sexo = :s;");
                $cmd->bindParam("n", $nome);
                $cmd->bindParam("e", $email);
                $cmd->bindParam("s", $sexo);              
				if($cmd->execute()){
					return true;
				}else{
					return false;
				}
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }

        function selecionarPnome($cond){
                try{
                    $conexao = new Conexao();   
                    $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario WHERE nome = :cond;");
                    $cmd->bindParam("cond", $cond);
                    $cmd->execute();
                    $resultado = $cmd->fetch(PDO::FETCH_OBJ);
                    return $resultado;
                    $conexao->fecharConexao();
                }catch(PDOException $e){
                    echo "Erro no banco: {$e->getMessage()}";
                }catch(Exception $e){
                    echo "Erro geral: {$e->getMessage()}";
                }
            }           
        
        function inserir($usuario){
            try{
                $conexao = new Conexao();
				$nome = $usuario->getNome();
				$email = $usuario->getEmail();
				$sexo = $usuario->getSexo();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO usuario(nome,email,sexo) values(:n,:e,:s);");
                $cmd->bindParam("n", $nome);
                $cmd->bindParam("e", $email);
                $cmd->bindParam("s", $sexo);               
				if($cmd->execute()){
					return true;
				}else{
					return false;
				}
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
	
     }
?>

