<?php		
require_once("Conect.php");
require_once("../modelo/senhaM.php");
class SenhaControle{	


		function remover($id){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("DELETE FROM senha WHERE id=:id;");
                $cmd->bindParam("id", $id);
                if($cmd->execute()){
                    return true;
                }else{
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro de PDO: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }



        function selecionarTodos(){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM senha;");
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Senha");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        
        
        function update($senha){
                try{
                    $conexao = new Conexao();
                    $senhaAtual = $senha->getSenhaAtual();
                    $senhaPassada = $senha->getSenhaPassada();
                    $userSenha = $senha->getUserSenha();
                    $cmd = $conexao->getConexao()->prepare("UPDATE senha SET senhaAtual = :sa, senhaPassada = :sp WHERE userSenha = :idUser;");
                    $cmd->bindParam("sa", $senhaAtual);
                    $cmd->bindParam("sp", $senhaPassada);
                    $cmd->bindParam("idUser", $userSenha);           
                    if($cmd->execute()){
                        $conexao->fecharConexao();
                        return true;
                    }else{
                        $conexao->fecharConexao();
                        return false;
                    }
                }catch(PDOException $e){
                    echo "Erro no banco: {$e->getMessage()}";
                }catch(Exception $e){
                    echo "Erro geral: {$e->getMessage()}";
                }
            }

        
        
        function inserir($senha){
            try{
                $conexao = new Conexao();
				$senhaAtual = $senha->getSenhaAtual();
				$userSenha = $senha->getUserSenha();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO senha(senhaAtual,userSenha) values(:a,:u);");
                $cmd->bindParam("a", $senhaAtual);
                $cmd->bindParam("u", $userSenha);
				if($cmd->execute()){
					return true;
				}else{
					return false;
				}
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }

        function selecionarPid($id){
            try{
                $conexao = new Conexao();   
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM senha WHERE id=:id;");
                $cmd->bindParam("id", $id);
                $cmd->execute();
                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        
        
        function confirmaCad($con, $sen){
        	if($sen == $con){
        		return true;
        	}else{
        		return false;
        	}
        }
        
        
        function confirmaLog($conf, $senha){
        	if($senha == $conf){
        		return true;
        	}else{
        		return false;
        	}
        }


		function criptografa($senha){
			$a=fopen("nach.txt","r");
			$c=fread($a,4096);
			$cifragem="AES-128-CBC";
			$tamanho=openssl_cipher_iv_length($cifragem);
			$tm=openssl_random_pseudo_bytes($tamanho);
			$criptografar=openssl_encrypt($senha,$cifragem,$c,OPENSSL_RAW_DATA,$tm);
			$criptografar=base64_encode($tm.$criptografar);
			return $criptografar;
		}
		
		
		function descriptografa($senhaCriptografada){
			$senhaCriptografada=base64_decode($senhaCriptografada);
			$a=fopen("nach.txt","r");
			$chave=fread($a,4096);
			$cifragem="AES-128-CBC";
			$tamanho=openssl_cipher_iv_length($cifragem);
			$tm=mb_substr($senhaCriptografada,0,$tamanho,'8bit');
			$senhaCriptografada=mb_substr($senhaCriptografada,$tamanho,null,'8bit');
			$desc=openssl_decrypt($senhaCriptografada,$cifragem,$chave,OPENSSL_RAW_DATA,$tm);
			return $desc;	
		}
	}
?>
