<?php
session_start();
if($_SESSION['nome']){
require_once("senhaC.php");
$senhaModelo = new Senha();
$senhaControl= new SenhaControle();
$dados=$senhaControl->selecionarPid($_SESSION['id']);
$senha = ((isset($_POST["senha"]))?$_POST["senha"] : null);

$sa = $senhaControl->criptografa($senha);

$senhaModelo->setSenhaAtual($sa);
$senhaModelo->setSenhaPassada($dados->senhaAtual);
$senhaModelo->setUserSenha($_SESSION['id']);

$senhaControl->update($senhaModelo);

header("Location: sair.php");

}else{
	header("Location: ../login.php");
}

?>