<?php
session_start();
if($_SESSION['nome']){
require_once("planFC.php");
$pfControl= new PlanControle();
$dados=$pfControl->selecionarTodosPoridUser($_SESSION['id']);

echo '   
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Historico PF</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/historico.css">
</head>
<body>
<div class="voltar container-fluid"> 
	<a href="../Perfil.php"> <- Voltar </a>
</div>
	<section>
		<div class="container-fluid">
			<div  class="container his">
				<div class=" txt-sd text-center mb-4">
					<h3 class="txt-saudacao">Historico</h3>
				</div>
					<div class="container-fluid">
					<div class="table-responsive">
						<table class="table">
	  						<thead>
							    <tr>
							      <th scope="col">Data</th>
							      <th scope="col">Capital</th>
							      <th scope="col">Contas</th>
							      <th scope="col">Poupança</th>
							      <th scope="col">Investir</th>
							      <th scope="col">Futilidades</th>
							 	  <th scope="col">Emergências/Projetos</th>
							      <th scope="col">Deletar</th>
							    </tr>
	  						</thead>
	  						<tbody>';
								foreach($dados as $campos){
									$id = $campos->getId();
	            					echo "<tr>";
		    								echo "<td>{$campos->getData()}</td>";
		    								echo "<td>{$campos->getCapital()}</td>";
		    								echo "<td>{$campos->getContas()}</td>";
		    								echo "<td>{$campos->getPoupanca()}</td>";
		    								echo "<td>{$campos->getInvestimento()}</td>";
		    								echo "<td>{$campos->getFutilidades()}</td>";
		    								echo "<td>{$campos->getEmepro()}</td>";
		    								echo"<td><a onclick='return confirm(\"Deseja mesmo excluir o post?\")' href='removerPF.php?id={$campos->getId()}' class='bot-del'>Deletar</a></td>";
    								echo "</tr>";
            							
        						}
							echo '
		  					</tbody>
						</table>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section>';

	require_once("../footer.php");
echo '   
</body>
</html>

';
}else{
	header("Location: ../login.php");
}

?>
