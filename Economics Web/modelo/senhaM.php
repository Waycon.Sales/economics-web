<?php
	class Senha{
		private $id;
		private $senhaAtual;
		private $senhaPassada;
		private $userSenha;
		public function getId(){
            return $this->id;
    	}
        public function setId($i){
    	    $this->id = $i;
    	}
		public function getSenhaAtual(){
			return $this->senhaAtual;
		}
		public function setSenhaAtual($a){
			$this->senhaAtual = $a;
		}
		public function getSenhaPassada(){
			return $this->senhaPassada;
		}
		public function setSenhaPassada($p){
			$this->senhaPassada = $p;
		}
		public function getUserSenha(){
			return $this->userSenha;
		}
		public function setUserSenha($u){
			$this->userSenha = $u;
		}
	}
?>
