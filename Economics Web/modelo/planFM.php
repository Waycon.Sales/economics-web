<?php
	class PlanoFinanceiro{
		private $id;
		private $capital;
		private $contas;
		private $poupanca;
		private $userPlano;
		private $futilidades;
		private $investimento;
		private $emepro;
		private $data;
		public function getId(){
	            return $this->id;
       	}
	    public function setId($i){
        	    $this->id = $i;
	    }

		public function getCapital(){
			return $this->capital;
		}
		public function setCapital($c){
			$this->capital = $c;
		}
		public function getContas(){
			return $this->contas;
		}
		public function setContas($c){
			$this->contas = $c;
		}
		public function getPoupanca(){
			return $this->poupanca;
		}
		public function setPoupanca($p){
			$this->poupanca = $p;
		}
		public function getUserPlano(){
			return $this->userPlano;
		}
		public function setUserPlano($u){
			$this->userPlano = $u;
		}
		public function getFutilidades(){
			return $this->futilidades;
		}
		public function setFutilidades($f){
			$this->futilidades = $f;
		}
		public function getInvestimento(){
			return $this->investimento;
		}
		public function setInvestimento($i){
			$this->investimento = $i;
		}
		public function getEmepro(){
			return $this->emepro;
		}
		public function setEmepro($e){
			$this->emepro = $e;
		}
		public function getData(){
			return $this->data;
		}
		public function setData($d){
			$this->data = $d;
		}
	}
?>
